********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Apache Solr Views Spelling Module
Author: Robert Castelo <www.codepositive.com>
Drupal: 7.x
********************************************************************
DESCRIPTION:

Provides option to display a spelling suggestion after a keyword search on a Views generated 
page that gets it’s results from Apache Solr.



********************************************************************
PREREQUISITES:

  Apache Solr Search
  https://www.drupal.org/project/apachesolr

  Views
  https://www.drupal.org/project/views

  Apache Solr Views
  https://www.drupal.org/project/apachesolr_views


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/


2. Enable the module by navigating to:

   administer > modules

  Click the 'Save configuration' button at the bottom to commit your
  changes.



********************************************************************
USAGE

On a View which is set to display as a Page, click on the Other field group, and then click on 
‘Spelling suggestions’ link.

The options are documented on the form that pops up.

Once a View that has Spelling suggestions enabled and configured the spelling suggestions will become available on a block.

The block admin title begins with ‘Apache Solr Views Spelling:’.

Add the block to the result pages using the administer block config, or Panels, or Context.


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   https://www.drupal.org/project/views_linker

- Comission New Features:
   https://www.drupal.org/user/3555/contact

- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F


********************************************************************
SPONSORS

Code Positive
http://www.codepositive.com
