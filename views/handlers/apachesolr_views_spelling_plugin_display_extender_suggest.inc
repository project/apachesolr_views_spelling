<?php
/**
 * @file
 * Custom display extender plugin for Views.
 */


/**
 * The plugin that added additional setting to views edit form.
 *
 * @ingroup views_display_plugins
 */
class apachesolr_views_spelling_plugin_display_extender_suggest extends views_plugin_display_extender {
  /**
   * Provide a form to edit options for this plugin.
   */
  function options_definition_alter(&$options) {

    $options['apachesolr_views_spelling_enabled'] = array(
      'default' => 0,
      'bool'    => TRUE,
    );

    $options['suggestion_label'] = array(
      'default'      => '',
      'translatable' => TRUE,
    );

    $options['apachesolr_views_spelling_block_delta'] = array(
      'default' => '',
    );

  }

  function options_summary(&$categories, &$options) {

    $enabled = $this->display->get_option('apachesolr_views_spelling_enabled');

    $options['apachesolr_views_spelling'] = array(
      'category' => 'other',
      'title'    => t('Spelling suggestions'),
      'value'    => empty($enabled) ? t('Unset') : t('Enabled'),
    );

  }

  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);

    if ($form_state['section'] == 'apachesolr_views_spelling') {

      $form['#title'] .= t('Spelling suggestion options');
      $enabled          = $this->display->get_option('apachesolr_views_spelling_enabled');
      $suggestion_label = $this->display->get_option('suggestion_label');

      $form['enabled'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Enable'),
        '#description'   => t('Enable the spelling suggestion option for this view display.'),
        '#default_value' => empty($enabled) ? FALSE : $enabled,
      );

      $form['search']['suggestion_label'] = array(
        '#type'          => 'textfield',
        '#required'      => TRUE,
        '#title'         => t('Suggestion label'),
        '#description'   => t('Text to display on the suggestions label.'),
        '#default_value' => empty($suggestion_label) ? t('Did you mean?') : $suggestion_label,
      );

      $form['suggestion_label']['#type'] = 'container';
    }

  }

  function options_submit(&$form, &$form_state) {

    if ($form_state['section'] == 'apachesolr_views_spelling') {
      $view_name   = $this->view->name;
      $display     = $this->view->current_display;
      $settings    = $form_state['values'];
      $block_delta = $this->display->get_option('apachesolr_views_spelling');

      if (!empty($settings['enabled']) && empty($block_delta)) {
        $block_delta = md5($view_name . '::' . $display);
      }

      $this->display->set_option('apachesolr_views_spelling_enabled', $settings['enabled']);
      $this->display->set_option('suggestion_label', $settings['suggestion_label']);
      $this->display->set_option('apachesolr_views_spelling', $block_delta);
    }
  }

  /**
   * Add this filter to the query.
   */
//  public function query() {
//    if (!empty($this->value)) {
//      $this->query->set_query($this->value);
//
//      if ($this->options['spellcheck']) {
//        $this->query->spellcheck();
//      }
//    }
//  }

}
