<?php
/**
 * @file
 * Integration with Views.
 */

/**
 * Implements hook_views_plugins().
 */
function apachesolr_views_spelling_views_plugins() {
  $path = drupal_get_path('module', 'apachesolr_views_spelling');
  $plugins = array();
  $plugins['display_extender']['apachesolr_views_spelling'] = array(
    'title' => t('Spelling suggestions'),
    'help' => t('Spelling suggestion for Apache Solr Views.'),
    'path' => $path . '/views/handlers',
    'handler' => 'apachesolr_views_spelling_plugin_display_extender_suggest',
    'uses options' => TRUE,
    'enabled' => TRUE,
  );

  return $plugins;
}

